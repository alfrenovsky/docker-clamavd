FROM alpine:latest

MAINTAINER Alfredo Rezinovsky <alfrenovsky@gmail.com>

RUN apk --update --no-cache -v --virtual clamav add clamav-daemon clamav-libunrar && \
    for dir in /var/lib/clamav /var/run/clamav; do mkdir -p "${dir}"; chown clamav:clamav "${dir}"; chmod 750 "${dir}"; done

# This link allows generated clamd.conf to be in
# /var/lib/clamav wich lives in shared volume.
RUN rm /etc/clamav/clamd.conf
RUN ln -s /var/lib/clamav/clamd.conf /etc/clamav/

ADD clamd.sh /
ADD freshclam.sh /
RUN chmod a+x /*.sh

EXPOSE 3310

CMD ["/clamd.sh"]
