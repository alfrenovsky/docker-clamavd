#!/bin/sh

if [ -z "${CLAMD_HOST}" ]
then
  echo "Enviroment variable CLAMD_HOST must be set to the CLAMAV container host"
  echo "This is needed for freshclam to send notifications to this host"
  echo ""
  exit 1
fi

DATADIR="/var/lib/clamav"

# Strictly clamav needs all 3 files to work
# but with the first cvd can boot and then reload
# the databases when they all arrive.
# This is important ONLY the first run cause after that
# databases are kept in a persistent volume

# FILES="daily.cvd main.cvd bytecode.cvd"
FILES="daily.cvd"

echo "Generating clamd.conf..."
cat << EOF > /var/lib/clamav/clamd.conf.tmp
LogTime yes
LogVerbose yes
PidFile /run/clamav/clamd.pid
TCPSocket 3310
TCPAddr $CLAMD_HOST
Foreground yes
EOF
mv /var/lib/clamav/clamd.conf.tmp /var/lib/clamav/clamd.conf
echo "done."

for file in $FILES
do
  path="${DATADIR}/${file}"
  if [ ! -f "${path}" ]
  then
    echo "Waiting for ${file}..."
    until [ -f "${path}" ]
    do
      sleep 1
    done
  fi
  echo "${file} file is in place."
done

echo "Starting clamav daemon"
clamd
