#!/bin/sh

# Poor's man race condition avoider
#sleep 3

DATADIR="/var/lib/clamav"
FILES="clamd.conf"
FILES=""

rm ${DATADIR}/tmp/* 2> /dev/null

echo "Generating freshclam.conf..."
cat << EOF > /etc/clamav/freshclam.conf.tmp
LogTime yes
PidFile /run/clamav/freshclam.pid
DatabaseMirror database.clamav.net
NotifyClamd /etc/clamav/clamd.conf
Foreground yes
Debug yes
ReceiveTimeout 1000
EOF
mv /etc/clamav/freshclam.conf.tmp /etc/clamav/freshclam.conf
echo "Done."

for file in $FILES
do
  path="${DATADIR}/${file}"
  if [ ! -f "${path}" ]
  then
    echo "Waiting for ${path}..."
    until [ -f "${path}" ]
    do
      sleep 1
    done
  fi
  echo "${file} is in place."
done

echo "Starting freshclam daemon"
freshclam -d
